'use strict'

let reekoh = require('reekoh')
let plugin = new reekoh.plugins.Service()
let serviceClient = null

const request = require('request')

plugin.on('data', (data) => {
  let headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Cache-Control': 'no-cache'
  }

  let body = {
    'client_id': plugin.config.clientId,
    'client_secret': plugin.config.clientSecret,
    resource: plugin.config.resource,
    username: plugin.config.username,
    password: plugin.config.password,
    'grant_type': plugin.config.grantType || 'password'
  }

  let options = {
    url: plugin.config.oauth2TokenEndpoint,
    method: 'POST',
    headers: headers,
    form: body,
    json: true
  }

  request(options, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      plugin.pipe(data, body)
      .then(() => {
        plugin.log({
          title: 'Azure Active Directory Service',
          result: body
        })
      })
      .catch(plugin.logException)
    } else {
      plugin.logException(error)
    }
  })
})

plugin.once('ready', () => {
  plugin.log('Azure Active Directory Service has been initialized.')
  plugin.emit('init')
})

process.on('SIGINT', () => {
  // Do graceful exit
  serviceClient.close()
})

module.exports = plugin
