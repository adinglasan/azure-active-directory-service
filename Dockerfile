FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/azure-active-directory-service

WORKDIR /home/node/azure-active-directory-service

# Install dependencies
RUN npm install pm2@2.6.1 -g

CMD pm2-docker --json app.yml

